"use strict";
const express = require("express");
const fs = require('fs').promises;
const cors = require('cors')
const bodyParser = require('body-parser')
const libre = require("libreoffice-convert");
libre.convertAsync = require("util").promisify(libre.convert);
const app = express();
const port = 4000;
app.use(cors())
app.use(bodyParser.json())

app.get("/", (req, res) => {
  if (!req.query.file) {
    return res.status(400).json({ error: "Файлын илгээнүү" });
  }
  convertToPdf(req.query.file)
    .then((rs) => {
      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader('Content-Disposition', `attachment; filename=Converted Pdf.pdf`);
      res.send(rs);
    })
    .catch(function (err) {
      console.log(`Error converting file: ${err}`);
    });
});

app.listen(port, () => {
  console.log(`Running on ${port}`);
});

async function convertToPdf(path) {
  // /var/www/html
  // ../tubis/public
  const laravelPath = '/var/www/html/public'
  const docxBuf = await fs.readFile(`${laravelPath}${path}`);
  let pdfBuf = await libre.convertAsync(docxBuf, '.pdf', undefined);
  return pdfBuf;
}
